package com.gepeq.transporter;

import com.gepeq.transporter.receiver.FileReceiverServer;
import com.gepeq.transporter.transmitter.FileTransmitter;

import java.io.File;

public class Transporter {
    public static void main(String[] args) {
        System.out.println("Transporter staring...");
        if(args.length == 0){
            throw new RuntimeException("No arguments specified!");
        }

        String mode = args[0];
        if("receiver".equals(mode)){
            int port = 9955;
            System.out.println("Starting file receiver in port " + port);
            FileReceiverServer fileReceiverServer = new FileReceiverServer(port);
            fileReceiverServer.start();
        }

        if("sender".equals(mode)){
            String host = args[1];
            Integer port = Integer.parseInt(args[2]);
            String filename = args[3];
            FileTransmitter fileTransmitter = new FileTransmitter(new File(filename), host, port);
            fileTransmitter.startTransfer();
        }
    }
}
