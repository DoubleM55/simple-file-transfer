package com.gepeq.transporter.receiver;

import com.gepeq.transporter.data.InfoHeader;
import com.google.gson.Gson;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;

public class FileReceiver {
    private final InputStream inputStream;

    public FileReceiver(InputStream inputStream) {
        this.inputStream = inputStream;
    }

    public void startTransfer() {
        Thread thread = new Thread(new StreamToFileDumper(inputStream));
        thread.start();
    }

    private class StreamToFileDumper implements Runnable {
        private final InputStream inputStream;
        private final Gson gson = new Gson();
        private final File outputDir;
        private int bufferSize = 10000;

        private StreamToFileDumper(InputStream inputStream) {
            String property = System.getProperty("buffer.size");
            if(property != null && !property.trim().isEmpty()){
                int bufferSize = Integer.parseInt(property);
                System.out.println("Using buffer size: " + bufferSize);
                this.bufferSize = bufferSize;
            }

            this.inputStream = inputStream;
            final String userDirPath = System.getProperty("user.dir");
            System.out.println("Received files will be stored in: " + userDirPath);
            this.outputDir = new File(userDirPath);
        }

        public void run() {
            try {
                int headerSize = getHeaderSize();
                System.out.println("Got header size: " + headerSize);
                String headerString = getHeaderString(headerSize);
                System.out.println("Got header string: " + headerString);
                final InfoHeader infoHeader = deserializeHeader(headerString);
                System.out.println("Deserialized header: " + infoHeader);

                final File outputFile = new File(this.outputDir, infoHeader.getFileName());
                FileOutputStream fileOutputStream = getFileOutputStream(outputFile);
                final byte[] buffer = new byte[bufferSize];
                long bytesReadTotal = 0;
                final long fileSize = infoHeader.getFileSize();
                System.out.println("Transfer started");
                while (bytesReadTotal < fileSize) {
                    final long remainingBytes = fileSize - bytesReadTotal;
                    final int readBytes = readBytes(inputStream, buffer, remainingBytes <= bufferSize ? (int)remainingBytes : bufferSize);
                    writeBufferToFile(fileOutputStream, buffer, readBytes);
                    bytesReadTotal += readBytes;
                }
                closeFile(fileOutputStream);
                System.out.println("Read " + bytesReadTotal + " of " + fileSize);
                System.out.println("Transfer complete!");
            } catch (Exception e) {
                System.out.println("Transfer failed: " + e.toString());
            } finally {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        private void closeFile(FileOutputStream fileOutputStream) {
            try {
                fileOutputStream.close();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }

        private void writeBufferToFile(FileOutputStream fileOutputStream, byte[] buffer, int readBytes) {
            try {
                fileOutputStream.write(buffer, 0, readBytes);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }

        private FileOutputStream getFileOutputStream(File outputFile) {
            try {
                return new FileOutputStream(outputFile);
            } catch (FileNotFoundException e) {
                throw new RuntimeException(e);
            }
        }

        private FileWriter getFileWriter(File outputFile) {
            try {
                return new FileWriter(outputFile);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }

        private InfoHeader deserializeHeader(String headerString) {
            return gson.fromJson(headerString, InfoHeader.class);
        }

        private String getHeaderString(int headerSize) {
            byte[] headerBytes = readBytes(inputStream, headerSize);
            return new String(headerBytes, Charset.forName("UTF-8"));
        }

        private int getHeaderSize() {
            byte[] headerSizeBytes = readBytes(inputStream, 4);
            ByteBuffer byteBuffer = ByteBuffer.wrap(headerSizeBytes);
            return byteBuffer.getInt();
        }

        private byte[] readBytes(InputStream inputStream, int n) {
            byte[] headerSizeBytes = new byte[n];
            try {
                int readCount = inputStream.read(headerSizeBytes);
                if (readCount != n) {
                    throw new IllegalStateException("Incorrect read size! " + readCount + ", should be: " + n);
                }
                return headerSizeBytes;
            } catch (IOException e) {
                throw new RuntimeException("Can't read bytes", e);
            }
        }

        private int readBytes(InputStream inputStream, byte[] buffer, int l) {
            try {
                return inputStream.read(buffer, 0, l);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }

    }
}
