package com.gepeq.transporter.receiver;

import java.io.IOException;
import java.io.InputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class FileReceiverServer {
    private final int port;

    public FileReceiverServer(int port) {
        this.port = port;
    }

    public void start(){
        ServerSocket serverSocket = getServerSocket(port);
        try {
            while (true) {
                Socket socket = acceptConnection(serverSocket);
                InputStream inputStream = socket.getInputStream();
                FileReceiver fileReceiver = new FileReceiver(inputStream);
                fileReceiver.startTransfer();
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                serverSocket.close();
            } catch (IOException e) {
                System.out.println("Can't close server socket: " + e.toString());
            }
        }
    }

    private Socket acceptConnection(ServerSocket serverSocket) {
        try {
            return serverSocket.accept();
        } catch (IOException e) {
            throw new RuntimeException("Error while waiting for connection", e);
        }
    }

    private ServerSocket getServerSocket(int port) {
        try {
            return new ServerSocket(port);
        } catch (IOException e) {
            throw new RuntimeException("Can't create server socket", e);
        }
    }

}
