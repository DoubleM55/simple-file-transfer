package com.gepeq.transporter.transmitter;

import com.gepeq.transporter.data.InfoHeader;
import com.google.gson.Gson;

import java.io.*;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;

public class FileTransmitter {
    private final File sourceFile;
    private final String destinationHost;
    private final int destinationPort;

    public FileTransmitter(File sourceFile, String destinationHost, int destinationPort) {
        this.sourceFile = sourceFile;
        this.destinationHost = destinationHost;
        this.destinationPort = destinationPort;
    }

    public void startTransfer(){
        Thread thread = new Thread(new FileSender());
        thread.start();
    }

    @Override
    public String toString() {
        return "FileTransmitter{" +
                "destinationHost='" + destinationHost + '\'' +
                ", sourceFile=" + sourceFile +
                ", destinationPort=" + destinationPort +
                '}';
    }

    private class FileSender implements Runnable{
        private final Gson gson = new Gson();
        private int bufferSize = 10000;

        public FileSender() {
            String property = System.getProperty("buffer.size");
            if(property != null && !property.trim().isEmpty()){
                int bufferSize = Integer.parseInt(property);
                System.out.println("Using buffer size: " + bufferSize);
                this.bufferSize = bufferSize;
            }
        }

        public void run() {
            final long startTime = System.currentTimeMillis();

            InfoHeader infoHeader = getInfoHeader();
            System.out.println("Constructed info header: " + infoHeader);
            String headerString = gson.toJson(infoHeader);
            System.out.println("Serialized header: " + headerString);
            byte[] headerBytes = headerString.getBytes(Charset.forName("UTF-8"));
            int headerLength = headerBytes.length;
            System.out.println("Header length: " + headerLength);

            FileInputStream fileInputStream = getFileInputStream();

            Socket socket = getSocket();
            OutputStream outputStream = getOutputStream(socket);

            writeHeaderLength(headerLength, outputStream);
            writeToStream(outputStream, headerBytes, headerBytes.length);

            byte[] buffer = new byte[bufferSize];
            long totalReadTotal = 0;
            long fileSize = sourceFile.length();
            try{
                while (totalReadTotal < fileSize){
                    long remainingBytes = fileSize - totalReadTotal;
                    int readBytes = readBytes(fileInputStream, buffer, remainingBytes <= bufferSize ? (int)remainingBytes : bufferSize);
                    writeToStream(outputStream, buffer, readBytes);
                    totalReadTotal += readBytes;

                    logStatistics(startTime, totalReadTotal, fileSize);
                }
            }catch (Exception e){
                System.out.println("Error while sending file: " + e);
            }finally {
                try {
                    fileInputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                try {
                    outputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                System.out.println("Transfer completed in: " + (System.currentTimeMillis() - startTime) / 1000.0f + " s");
            }


        }

        private void logStatistics(long startTime, long totalReadTotal, long fileSize) {
            long currentTime = System.currentTimeMillis();
            long elapsedMs = currentTime - startTime;
            float elapsedSeconds = elapsedMs / 1000.0f;
            float bytesPerSecond = totalReadTotal / elapsedSeconds;
            float kbps = bytesPerSecond / 1024.0f;
            float mbps = kbps / 1024.0f;
            System.out.println("Sent " + totalReadTotal + " of " + fileSize + ", speed: " + kbps + " kb/s, " + mbps + " mb/s");
        }

        private void writeHeaderLength(int headerLength, OutputStream outputStream) {
            ByteBuffer byteBuffer = ByteBuffer.allocate(4);
            byteBuffer.putInt(headerLength);
            byte[] headerSizeBytes = byteBuffer.array();
            writeToStream(outputStream, headerSizeBytes, headerSizeBytes.length);
        }

        private void writeToStream(OutputStream outputStream, byte[] buffer, int n) {
            try {
                outputStream.write(buffer);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }

        private OutputStream getOutputStream(Socket socket) {
            try {
                return socket.getOutputStream();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }

        private Socket getSocket() {
            try {
                return new Socket(destinationHost, destinationPort);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }

        private InfoHeader getInfoHeader() {
            final long fileSize = sourceFile.length();
            final String fileName = sourceFile.getName();
            return new InfoHeader(fileName, fileSize);
        }

        private FileInputStream getFileInputStream() {
            try {
                return new FileInputStream(sourceFile);
            } catch (FileNotFoundException e) {
                throw new RuntimeException(e);
            }
        }

        private byte[] readBytes(InputStream inputStream, int n) {
            byte[] headerSizeBytes = new byte[n];
            try {
                int readCount = inputStream.read(headerSizeBytes);
                if (readCount != n) {
                    throw new IllegalStateException("Incorrect read size! " + readCount + ", should be: " + n);
                }
                return headerSizeBytes;
            } catch (IOException e) {
                throw new RuntimeException("Can't read bytes", e);
            }
        }

        private int readBytes(InputStream inputStream, byte[] buffer, int size) {
            try {
                return inputStream.read(buffer, 0, size);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
