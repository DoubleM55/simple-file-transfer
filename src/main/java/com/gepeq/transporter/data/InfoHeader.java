package com.gepeq.transporter.data;

public class InfoHeader {
    private final String fileName;
    private final long fileSize;

    public InfoHeader(String fileName, long fileSize) {
        this.fileName = fileName;
        this.fileSize = fileSize;
    }

    public String getFileName() {
        return fileName;
    }

    public long getFileSize() {
        return fileSize;
    }

    @Override
    public String toString() {
        return "InfoHeader{" +
                "fileName='" + fileName + '\'' +
                ", fileSize=" + fileSize +
                '}';
    }
}
