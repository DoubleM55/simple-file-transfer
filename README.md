# Simple file transfer #

Send / Receive files over TCP protocol.

## Usage ##
- Build JAR
```
#!cmd

mvn clean install
```

- Start server: 
```
#!cmd

java -Dbuffer.size=100000 -jar gepeq-transporter-1.0-SNAPSHOT.jar receiver
```
- Send files to server: 
```
#!cmd

java -Dbuffer.size=100000 -jar target/gepeq-transporter-1.0.0-SNAPSHOT.jar sender 192.168.5.9 9955 "C:\Users\mmatosevic\Downloads\wat.mov"
```